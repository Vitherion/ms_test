package com.example.spt.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.stereotype.Service;
import com.example.spt.model.Document;
import com.example.spt.model.Page;

@Service
public class DocumentRepository {
	
	public List<Document> getDocumentListFor(int numDocuments, int numPages){
		List<Document> documents = new ArrayList<>();
		
		for (int i = 0; i < numDocuments; ++i) {
            String documentName = "Document" + (i + 1);
            List<Page> pages = new ArrayList<>();

            for (int j = 0; j < numPages; ++j) {
                int pageNum = j + 1;
                int linesNum = new Random().nextInt(10) + 1;
                Page page = new Page(pageNum, linesNum);
                pages.add(page);
            }

            Document document = new Document(documentName, pages);
            documents.add(document);
        }
		
		return documents;
	}
	
	public List<Document> getDocumentListForStream(int numDocuments, int numPages){
		return IntStream.range(0, numDocuments)
                .mapToObj(i -> {
                    String documentName = "Document" + (i + 1);
                    List<Page> pages = IntStream.range(0, numPages)
                            .mapToObj(j -> new Page(j + 1, new Random().nextInt(10) + 1))
                            .collect(Collectors.toList());

                    return new Document(documentName, pages);
                })
                .collect(Collectors.toList());
	}

}
