package com.example.spt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsJavaApplication.class, args);
	}

}
