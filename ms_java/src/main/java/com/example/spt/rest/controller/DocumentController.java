package com.example.spt.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.spt.model.ResponseDocument;
import com.example.spt.service.DocumentService;

@RestController
@RequestMapping("/rest/v1")
public class DocumentController {
	
	@Autowired
	DocumentService documentService;
	
	@GetMapping("/getDocumentsLinesAndMaxFor/{number}")
	public List<ResponseDocument> getDocumentsLinesAndMaxFor(@PathVariable int number){
		return documentService.getDocumentsLinesAndMaxFor(number);
	}
	
	@GetMapping("/getDocumentsLinesAndMaxStream/{number}")
	public List<ResponseDocument> getDocumentsLinesAndMaxStream(@PathVariable int number){
		return documentService.getDocumentsLinesAndMaxStream(number);
	}
}
