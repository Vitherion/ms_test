package com.example.spt.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spt.model.Document;
import com.example.spt.model.Page;
import com.example.spt.model.ResponseDocument;
import com.example.spt.repository.DocumentRepository;

@Service
public class DocumentService {
	
	@Autowired
	DocumentRepository documentRepository;
	
	private int numPages = 10;
	
	public List<ResponseDocument> getDocumentsLinesAndMaxFor(int numDocuments) {
		
		List<Document> documents = documentRepository.getDocumentListFor(numDocuments, numPages);

		return calculateLineInfoWithFor(documents);
	}
	
	public List<ResponseDocument> getDocumentsLinesAndMaxStream(int numDocuments) {
		
		List<Document> documents = documentRepository.getDocumentListForStream(numDocuments, numPages);
		
		return calculateLineInfoWithStreams(documents);
	}
	
	private static List<ResponseDocument> calculateLineInfoWithFor(List<Document> documents) {
        List<ResponseDocument> resultList = new ArrayList<>();

        for (Document document : documents) {
            int totalLines = 0;
            int maxLinesPage = -1;
            int maxLines = -1;

            for (Page page : document.getPages()) {
                totalLines += page.getLinesNum();

                if (page.getLinesNum() > maxLines) {
                    maxLines = page.getLinesNum();
                    maxLinesPage = page.getPageNum();
                }
            }

            resultList.add(new ResponseDocument(document.getName(), totalLines, maxLinesPage, maxLines));
        }

        return resultList;
    }

	private static List<ResponseDocument> calculateLineInfoWithStreams(List<Document> documents) {
	    return documents.parallelStream() // Utilizar parallelStream() para permitir paralelización
	            .map(document -> {
	                int totalLines = document.getPages().parallelStream()
	                        .mapToInt(Page::getLinesNum)
	                        .sum();

	                Page maxLinesPage = document.getPages().parallelStream()
	                        .max(Comparator.comparingInt(Page::getLinesNum))
	                        .orElse(null);

	                return new ResponseDocument(
	                        document.getName(),
	                        totalLines,
	                        maxLinesPage != null ? maxLinesPage.getPageNum() : -1,
	                        maxLinesPage != null ? maxLinesPage.getLinesNum() : -1
	                );
	            })
	            .collect(Collectors.toList());
	}
}
