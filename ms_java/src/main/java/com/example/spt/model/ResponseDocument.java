package com.example.spt.model;

public class ResponseDocument {
	
	private String documentName;
	private int totalLines;
	private int maxLinesPage;
	private int maxLines;

    public ResponseDocument(String documentName, int totalLines, int maxLinesPage, int maxLines) {
    	this.documentName = documentName;
        this.totalLines = totalLines;
        this.maxLinesPage = maxLinesPage;
        this.maxLines = maxLines;
    }
    
	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public int getTotalLines() {
		return totalLines;
	}

	public void setTotalLines(int totalLines) {
		this.totalLines = totalLines;
	}

	public int getMaxLinesPage() {
		return maxLinesPage;
	}

	public void setMaxLinesPage(int maxLinesPage) {
		this.maxLinesPage = maxLinesPage;
	}

	public int getMaxLines() {
		return maxLines;
	}

	public void setMaxLines(int maxLines) {
		this.maxLines = maxLines;
	}
    
}
