package com.example.spt.model;

public class Page {
	
	private int pageNum;
	private int linesNum;
	
	public Page(int pageNum, int linesNum) {
        this.pageNum = pageNum;
        this.linesNum = linesNum;
    }
	
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getLinesNum() {
		return linesNum;
	}
	public void setLinesNum(int linesNum) {
		this.linesNum = linesNum;
	}
	
}
