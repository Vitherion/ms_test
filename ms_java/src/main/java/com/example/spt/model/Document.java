package com.example.spt.model;

import java.util.List;

public class Document {
	
	private String name;
	private List<Page> pages;
	
	public Document(String name, List<Page> pages) {
        this.name = name;
        this.pages = pages;
    }
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Page> getPages() {
		return pages;
	}
	public void setPages(List<Page> pages) {
		this.pages = pages;
	}
	
}
